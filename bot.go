package bot

import "github.com/gempir/go-twitch-irc/v2"

type Client interface {
	Connect() error
	Depart(channel string)
	Disconnect() error
	Join(channels ...string)
	OnConnect(callback func())
	OnPrivateMessage(callback func(message twitch.PrivateMessage))
	Say(channel string, msg string)
}

type Bot struct {
	Channel string
	Client
	Joined bool
}

func NewBot(client Client) *Bot {
	return &Bot{
		Client: client,
	}
}

func (b *Bot) Connect() error {
	err := b.Client.Connect()
	if err != nil {
		return err
	}
	b.Joined = true
	return nil
}

func (b *Bot) Disconnect() error {
	err := b.Client.Disconnect()
	if err != nil {
		return err
	}
	b.Joined = false
	return nil
}

func (b *Bot) Join() {
	b.Client.Join(b.Channel)
	b.Joined = true
}

func (b *Bot) Leave() {
	b.Client.Depart(b.Channel)
	b.Joined = false
}

func (b *Bot) OnConnect(callback func()) {
	b.Client.OnConnect(callback)
}

func (b *Bot) OnPrivateMsg(callback func(message twitch.PrivateMessage)) {
	b.Client.OnPrivateMessage(callback)
}

func (b *Bot) Say(msg string) {
	b.Client.Say(b.Channel, msg)
}

func (b *Bot) SetChannel(channel string) {
	b.Channel = channel
}
