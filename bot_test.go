package bot

import (
	"github.com/gempir/go-twitch-irc/v2"
	"testing"
)

func assertCallCount(t *testing.T, c *fakeClient, amount int) {
	t.Helper()
	if c.CallCount != amount {
		switch amount {
		case 1:
			t.Errorf("expected %d client call, got %d", amount, c.CallCount)
		default:
			t.Errorf("expected %d client calls, got %d", amount, c.CallCount)
		}
	}
}

func TestConnect(t *testing.T) {
	client := fakeClient{}
	b := NewBot(&client)
	err := b.Connect()
	if err != nil {
		t.Errorf("not expecting and error, got %v", err)
	}
	assertCallCount(t, &client, 1)
	if !b.Joined {
		t.Error("expected to be joined")
	}
}

func TestDisconnect(t *testing.T) {
	client := fakeClient{}
	b := NewBot(&client)
	b.Connect()
	b.Disconnect()
	assertCallCount(t, &client, 2)
	if b.Joined {
		t.Error("not expecting to be joined")
	}
}

func TestJoin(t *testing.T) {
	client := fakeClient{}
	b := NewBot(&client)
	b.SetChannel("some-channel")
	b.Join()
	assertCallCount(t, &client, 1)
	if !b.Joined {
		t.Errorf("expected client to be joined")
	}
}

func TestLeave(t *testing.T) {
	client := fakeClient{}
	b := NewBot(&client)
	b.SetChannel("some-channel")
	b.Connect()
	b.Leave()
	assertCallCount(t, &client, 2)
	if b.Joined {
		t.Error("not expecting to be joined")
	}
}

func TestOnConnect(t *testing.T) {
	client := fakeClient{}
	b := NewBot(&client)
	b.SetChannel("some-channel")
	b.Connect()
	b.Join()
	b.OnConnect(func() {})
	assertCallCount(t, &client, 3)
}

func TestOnPrivateMessage(t *testing.T) {
	client := fakeClient{}
	b := NewBot(&client)
	b.SetChannel("some-channel")
	b.Connect()
	b.Join()
	b.OnPrivateMsg(func(message twitch.PrivateMessage) {})
	assertCallCount(t, &client, 3)
}

func TestSay(t *testing.T) {
	message := "hello"
	client := fakeClient{}
	b := NewBot(&client)
	b.SetChannel("some-channel")
	b.Say(message)
	assertCallCount(t, &client, 1)
}

func TestSetChannel(t *testing.T) {
	channel := "some-channel"
	client := fakeClient{}
	b := NewBot(&client)
	b.SetChannel(channel)

	if b.Channel != channel {
		t.Errorf("expected %v, got %v", b.Channel, channel)
	}
}

type fakeClient struct {
	CallCount int
}

func (f *fakeClient) reset() {
	f.CallCount = 0
}

func (f *fakeClient) tick() {
	f.CallCount += 1
}

func (f *fakeClient) Connect() error {
	f.tick()
	return nil
}

func (f *fakeClient) Depart(channel string) {
	f.tick()
}

func (f *fakeClient) Disconnect() error {
	f.tick()
	return nil
}

func (f *fakeClient) Join(channels ...string) {
	f.tick()
}

func (f *fakeClient) OnConnect(callback func()) {
	f.tick()
}

func (f *fakeClient) OnPrivateMessage(callback func(message twitch.PrivateMessage)) {
	f.tick()
}

func (f *fakeClient) Say(channel, msg string) {
	f.tick()
}
